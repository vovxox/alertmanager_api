import requests
import json


class Staytus(object):

    """
    public-website:
    """
    SERVICES = ['public-website', 'VMS', 'CMS', 'API']

    def __init__(self, token, secret, url):
        self.token = token
        self.secret = secret
        self.url = url
        self.headers = {
            'Content-Type': 'application/json',
            'X-Auth-Token': self.token,
            'X-Auth-Secret': self.secret
        }

    def update_status(self, id, message, current_action, status='degraded-performance', services=[], **kwargs):
        API_URL = '%s/issues/%s/updates' % (self.url, id)

        data = {
            'text': message,
            'current_action': current_action,
            'status': status,
            'services': self.services,
        }
        data.update(kwargs)

        # if no services then remove it from the payload
        if not services:
            data.pop('services')

        resp = requests.post(API_URL, headers=self.headers,
                             data=json.dumps(data))

        if not resp.ok:
            raise Exception(resp.content)

        return resp

    def create_incident(self, summary, status='degraded-performance', current_action='investigating', state='investigating', service=['web-application']):

        api_url = '%s/api/v1/issues/create' % self.url
        payload = {
            'title': summary,
            'status': status,
            'current_action': current_action,
            'state': state,
            'services': self.service
        }
        resp = requests.post(api_url, headers=self.headers,
                             data=json.dumps(payload))
        if not resp.ok:
            raise Exception(resp.content)

        return resp

    def dupliate_identifier(self, service):

        data = {
            'service': service
        }

        response = requests.post(self.url, headers=self.headers,
                                 data=json.dump(data).json())

        for issue in range(len(response['data'])):
            print(response['data'][issue])

    def new_incident(self, title, message, status='degraded-performance', current_action='investigating', services=[]):
        resp = self.create_incident(title=title, message=message, status=status,
                                    current_action=current_action, services=services)
        resp = self.set_status(title=title, message=message, status=status,
                               current_action=current_action, services=services)
        return resp

    def investigating(self, id, message, **kwargs):
        resp = self.update_status(
            id=id, message=message, current_action='investigating')
        return resp

    def identified(self, id, message, **kwargs):
        resp = self.update_status(
            id=id, message=message, current_action='identified')
        return resp

    def monitoring(self, id, message, status='operational', services=[], **kwargs):
        resp = self.update_status(
            id=id, message=message, status=status, current_action='monitoring')
        return resp

    def resolved(self, id, message, **kwargs):
        resp = self.update_status(
            id=id, message=message, status='operational', current_action='resolved')
        return resp
