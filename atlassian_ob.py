from jira import JIRA


class Atlassian(object):

    def __init__(self, jira_url, project, username, password):
        self.url = jira_url
        self.project = project
        self.username = username
        self.password = password
        self.jira_session = JIRA(basic_auth=(self.username, self.password),
                                 server=self.url)

    def create_jira_issue(self, summary, description):
        try:
            self.jira_session.create_issue(
                project=self.project, summary=summary, description=description, issuetype={'name': 'Task'})
            return "OK", 200
        except Exception:
            return "NOP", 400

    def jql(self, project, summary):
        search_query = 'project = "%s" and summary ~ "%s" and status != closed' % (
            project, summary)
        return self.jira_session.search_issues(search_query)
