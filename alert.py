from flask import Flask, request
import logging
from staytus_ob import Staytus
from atlassian_ob import Atlassian
from jinja2 import Template

app = Flask(__name__)
logging.basicConfig(format='%(levelname)s %(message)s', level=logging.DEBUG)
summary_tmpl = Template(r'''
{% for a in alerts if a.status == 'firing' -%}
  {% for k, v in a.annotations.items() -%}
  {{ v }}
  {% endfor %}
{% endfor %}
''')

description_tmpl = Template(r'''
h2. Common information
{% for k, v in commonAnnotations.items() -%}
* *{{ k }}*: {{ v }}
{% endfor %}
h2. Active alerts
{% for a in alerts if a.status == 'firing' -%}
_Annotations_:
  {% for k, v in a.annotations.items() -%}
* {{ k }} = {{ v }}
  {% endfor %}
_Labels_:
  {% for k, v in a.labels.items() -%}
* {{ k }} = {{ v }}
  {% endfor %}
[Source|{{ a.generatorURL }}]
----
{% endfor %}
alert_group_key={{ groupKey }}
''')

# def update_staytus(summary, description):


def parsingSummary(data):
    summary = summary_tmpl.render(data)
    description = description_tmpl.render(data)
    return summary, description


@app.route('/health')
def health():
    return "OK", 200


@app.route('/issues/create/<string:project>', methods=['POST'])
def raise_incident(project):
    data = request.get_json()
    jira_session = Atlassian(
        jira_url='', project=project, username='', password='')

    staytus_session = Staytus(token='',
                              secret='', url='')

    if jira_session.jql(project, parsingSummary(data)[0]):
        logging.info("already exist")
    else:
        """
        Create if not exists
        """
        jira_session.create_jira_issue(
            parsingSummary(data)[0], parsingSummary(data)[1])

        staytus_session.create_incident(parsingSummary(data)[0])

    return "OK", 200


def main():
    app.run(host='localhost')


if __name__ == "__main__":
    main()
